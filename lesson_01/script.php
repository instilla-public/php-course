<?php

// NOTE: to launch this script, type "php script.php YOUR_FIRST_NAME YOUR_SECOND_NAME YOUR_YEAR_OF_BIRTH"

// show all errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// catch command line parameters
$firstName = $argv[0];
$lastName = $argv[1];
$yearOfBirth = (integer) $argv[2];

// compute generated variables
$completeName = $firstName . " " . $lastName;
$thisYear = 2018;
$age = $thisYear - $yearOfBirth;

echo "Hello, my name is " . $completeName . " and I am " . $age . " years old.";
echo PHP_EOL;