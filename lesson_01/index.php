<?php

	// show all errors
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	// catch get parameters
	$firstName = $_GET['firstName'];
	$lastName = $_GET['lastName'];
	$yearOfBirth = $_GET['yearOfBirth'];
	$jobTitle = $_GET['jobTitle'];
	$company = $_GET['company'];

	// compute generated variables
	$completeName = $firstName . " " . $lastName;
	$thisYear = 2018;
	$age = $thisYear - $yearOfBirth;
	$exampleLink = "http://127.0.0.1:8080/?firstName=Paolo&lastName=Meola&yearOfBirth=1987&jobTitle=CTO&company=Instilla";

?>

<head>
	<title>Pagina personale di <?php echo $completeName; ?></title>
	<style type="text/css">
		.panel {
			width: 100%;
			max-width: 400px;
			border: 1px solid blue;
			padding: 10px;
		}
		.panel img {
			width: 100%;
		}
	</style>
</head>
<body>
	<h1>Questa è la pagina personale di <?php echo $completeName; ?></h1>
	<div class="panel">
		<p>Ciao sono <b><?php echo $firstName; ?></b> e ho <?php echo $age; ?> anni.</p>
		<p>Nella vita sono <?php echo $jobTitle?> in <?php echo $company ?></p>
		<img src="http://meng.uic.edu/wp-content/uploads/sites/92/2018/07/empty-profile.png" />
	</div>
	<p>
		<a href="https://www.google.com/search?q=<?php echo $firstName."+".$lastName; ?>">Clicca qui</a>
		per cercarmi su google!
	</p>
	<br/><br/>
	<p>
		Per vedere come comunicare con la pagina tramite i parametri GET, clicca al seguente link:<br/>
		<a href="<?php echo $exampleLink; ?>"><?php echo $exampleLink; ?></a><br/>
		(Controlla giusto che il "base address" sia corretto!)
	</p>
</body>
</html>

